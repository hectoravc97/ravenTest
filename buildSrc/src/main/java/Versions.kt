object Versions {

    /** General **/

    const val hilt = "2.45"
    const val fragment = "1.6.2"
    const val activity = "1.8.2"
    const val material = "1.11.0"
    const val appCompat = "1.6.1"
    const val lifeCycle = "2.7.0"
    const val coroutines = "1.7.1"
    const val kotlinCore = "1.12.0"
    const val room = "2.4.2"

    /** Testing **/

    const val junit = "4.13.2"
    const val espresso = "3.5.1"
    const val extJunit = "1.1.5"
    const val mockVersion = "1.12.2"
    const val coroutinesTest = "1.6.0"
    const val mockitoVersion = "4.0.0"
    const val archVersion = "2.2.0"

    /** Network **/

    const val gsonVersion = "2.9.0"
    const val retrofitVersion = "2.9.0"
    const val okhttpVersion = "5.0.0-alpha.6"

    /** Navigation **/

    const val navVersion = "2.5.1"
}
