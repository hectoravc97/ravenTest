plugins {
    id(ModulePlugin.MODULE_NAME)
}

android {
    namespace = "com.raven.core"
}

dependencies {
    di()
    room()
    viewmodel()
    general()
    testing()
}
