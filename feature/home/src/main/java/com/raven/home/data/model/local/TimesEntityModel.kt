package com.raven.home.data.model.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "times_popular_table")
data class TimesEntityModel (
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name="uri") var uri : String = "",
    @ColumnInfo(name="url") var url : String="",
    @ColumnInfo(name="_id") var id_Times : String = "",
    @ColumnInfo(name="asset_id") var asset_id : String="",
    @ColumnInfo(name="source") var source : String="",
    @ColumnInfo(name="published_date") var published_date : String="",
    @ColumnInfo(name="updated") var updated : String="",
    @ColumnInfo(name="section") var section : String="",
    @ColumnInfo(name="nytdsection") var nytdsection : String="",
    @ColumnInfo(name="adx_keywords") var adx_keywords : String="",
    @ColumnInfo(name="column") var column : String="",
    @ColumnInfo(name="byline") var byline : String="",
    @ColumnInfo(name="type") var type : String="",
    @ColumnInfo(name="title") var title : String="",
    @ColumnInfo(name="abstract") var abstract : String="",
    @ColumnInfo(name="eta_id") var eta_id : String="",
)