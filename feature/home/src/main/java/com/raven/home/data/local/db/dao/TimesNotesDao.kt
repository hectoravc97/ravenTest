package com.raven.home.data.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.raven.home.data.model.local.TimesEntityModel

@Dao
interface TimesNotesDao {
    @Query("SELECT * FROM times_popular_table")
    fun getTimesNotes():List<TimesEntityModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTimesNotes(governmentInstitutions: List<TimesEntityModel>)

    @Query("DELETE FROM times_popular_table")
    fun deleteAllTimesNotes()
}