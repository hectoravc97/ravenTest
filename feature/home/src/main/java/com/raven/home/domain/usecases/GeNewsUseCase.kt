package com.raven.home.domain.usecases

import com.raven.core.bases.BaseUseCase
import com.raven.home.domain.HomeDataSource
import com.raven.home.domain.mapper.GetNewsEntityMapper
import com.raven.home.domain.mapper.GetNewsMapper
import com.raven.home.domain.models.GetNewsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GeNewsUseCase @Inject constructor(
    private val dataSource: HomeDataSource,
    private val mapper: GetNewsMapper,
    private val mapperEntity : GetNewsEntityMapper
) : BaseUseCase<Unit, List<GetNewsModel>>() {

    override fun execute(params: Unit): Flow<List<GetNewsModel>> {
        return flow{
            dataSource.getNews()
                .catch {
                    dataSource.getNewsFromDB().collect{
                        emit(mapper.transformDomainToUI(it))
                    }
                }
                .collect{ governmentInstitutionsFromApi ->
                if(governmentInstitutionsFromApi.isNotEmpty()){
                    withContext(Dispatchers.IO){
                        dataSource.clearNews()
                        val mappetValue = mapperEntity.transFormationDomainToEntity(governmentInstitutionsFromApi)
                        dataSource.insertAllNews(mappetValue)
                    }

                    dataSource.getNewsFromDB().collect{
                        emit(mapper.transformDomainToUI(it))
                    }
                }
            }
        }
    }
}
