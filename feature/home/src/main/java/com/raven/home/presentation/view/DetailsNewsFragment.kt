package com.raven.home.presentation.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.raven.home.R
import com.raven.home.databinding.FragmentDetailsNewsBinding
import com.raven.home.presentation.viewmodel.HomeViewModel
import com.raven.home.utils.Utils
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DetailsNewsFragment : Fragment() {

    private lateinit var mBinding: FragmentDetailsNewsBinding
    private val homeViewModel  : HomeViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentDetailsNewsBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initValues()
    }

    private fun initValues() {

        val new = homeViewModel.getList()[homeViewModel.getIndex()]

        mBinding.txtTitleNew.text = new.title
        mBinding.txtDescriptionNew.text = new.assetId
        mBinding.txtPublicationNew.text = new.publishedDate
        mBinding.txtSectionNew.text = new.section
        mBinding.txtSourceNew.text = new.source
        mBinding.txtAuthorNew.text = new.byline
        mBinding.txtUrlNew.text = new.url

        mBinding.txtUrlNew.setOnClickListener {
            if(Utils.isNetworkAvailable(requireContext())){
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(new.url))
                startActivity(browserIntent)
            }else{
                Toast.makeText(requireContext(), requireContext().getString(R.string.do_not_have_connection_news_connect), Toast.LENGTH_SHORT).show()
            }
        }

    }


}