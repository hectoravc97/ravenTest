package com.raven.home.presentation.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.raven.home.databinding.ItemNewsLayoutBinding
import com.raven.home.domain.models.GetNewsModel

class NewsViewHolder (view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemNewsLayoutBinding.bind(view)
    var setOnclickItem : ((Int) -> Unit)? = null

    fun bind(getNewsModel: GetNewsModel, position: Int) {

        binding.tvDate.text = getNewsModel.publishedDate
        binding.tvNews.text = getNewsModel.title
        binding.tvDescription.text = getNewsModel.assetId

        binding.imgDetail.setOnClickListener {
            setOnclickItem?.invoke(position)
        }

    }
}
