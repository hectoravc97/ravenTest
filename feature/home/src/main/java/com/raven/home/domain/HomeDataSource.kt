package com.raven.home.domain

import com.raven.home.data.model.ResultsResponseModel
import com.raven.home.data.model.local.TimesEntityModel
import kotlinx.coroutines.flow.Flow

interface HomeDataSource {

    suspend fun getNews(): Flow<List<ResultsResponseModel>>

    suspend fun getNewsFromDB(): Flow<List<TimesEntityModel>>

    suspend fun insertAllNews(list: List<TimesEntityModel>)

    suspend fun clearNews()
}
