package com.raven.home.data

import com.raven.home.data.local.db.dao.TimesNotesDao
import com.raven.home.data.model.ResultsResponseModel
import com.raven.home.data.model.local.TimesEntityModel
import com.raven.home.data.remote.response.TimesResponse
import com.raven.home.data.remote.service.HomeService
import com.raven.home.domain.HomeDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val timesApiClient : HomeService,
    private val timesNotesDao: TimesNotesDao
) : HomeDataSource {

    override suspend fun getNews(): Flow<List<ResultsResponseModel>>  = flow {
        val response: TimesResponse = timesApiClient.getNews()
        emit(response.results)
    }.flowOn(Dispatchers.IO)


    override suspend fun getNewsFromDB(): Flow<List<TimesEntityModel>> = flow {
        val response = timesNotesDao.getTimesNotes()
        emit(response)
    }.flowOn(Dispatchers.IO)


    override suspend fun insertAllNews(list: List<TimesEntityModel>) {
        timesNotesDao.insertTimesNotes(list)
    }

    override suspend fun clearNews() {
        timesNotesDao.deleteAllTimesNotes()
    }
}
