package com.raven.home.domain.mapper

import com.raven.home.data.model.local.TimesEntityModel
import com.raven.home.domain.models.GetNewsModel
import javax.inject.Inject

class GetNewsMapper @Inject constructor() {

    suspend fun transformDomainToUI(params: List<TimesEntityModel>) :  List<GetNewsModel> {
        return params.map {
            GetNewsModel(
                uri = it.uri,
                url = it.url,
                id = it.id_Times,
                assetId = it.asset_id,
                source = it.source,
                publishedDate = it.published_date,
                updated = it.updated,
                section = it.section,
                nytdsection = it.nytdsection,
                adxKeywords =it.adx_keywords,
                column = it.column,
                byline = it.byline,
                type = it.type,
                title = it.title,
                etaId = it.eta_id,
                abstract = it.abstract
            )
        }

    }
}
