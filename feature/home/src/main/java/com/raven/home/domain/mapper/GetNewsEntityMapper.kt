package com.raven.home.domain.mapper

import com.raven.home.data.model.ResultsResponseModel
import com.raven.home.data.model.local.TimesEntityModel
import com.raven.home.domain.models.GetNewsModel
import javax.inject.Inject

class GetNewsEntityMapper @Inject constructor() {
    suspend fun transFormationDomainToEntity(params: List<ResultsResponseModel>) :  List<TimesEntityModel> {
        return params.map {
            TimesEntityModel(
                uri = it.uri.toString(),
                url = it.url.toString(),
                id_Times = it.id.toString(),
                asset_id = it.abstract.toString(),
                source = it.source.toString(),
                published_date = it.publishedDate.toString(),
                updated = it.updated.toString(),
                section = it.section.toString(),
                nytdsection = it.nytdsection.toString(),
                column = it.column.toString(),
                byline = it.byline.toString(),
                type = it.type.toString(),
                title = it.title.toString(),
                abstract = it.abstract.toString()
            )
        }
    }
}
