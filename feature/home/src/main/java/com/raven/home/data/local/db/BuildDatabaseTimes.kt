package com.raven.home.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.raven.home.data.local.db.dao.TimesNotesDao
import com.raven.home.data.model.local.TimesEntityModel

@Database(entities = [TimesEntityModel::class], version = 1 )
abstract class BuildDatabaseTimes : RoomDatabase(){
    abstract fun getTimesDAO(): TimesNotesDao
}