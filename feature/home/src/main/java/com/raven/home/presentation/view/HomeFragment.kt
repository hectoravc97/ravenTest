package com.raven.home.presentation.view

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.raven.home.R
import com.raven.home.databinding.HomeFragmentBinding
import com.raven.home.presentation.view.adapter.NewsAdapter
import com.raven.home.presentation.viewmodel.HomeViewModel
import com.raven.home.utils.Utils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val homeViewModel  : HomeViewModel by activityViewModels()
    private lateinit var mBinding: HomeFragmentBinding
    private lateinit var newsAdapter: NewsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = HomeFragmentBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()
    }

    override fun onResume() {
        super.onResume()

        if(homeViewModel.getList().isNullOrEmpty()){
            homeViewModel.fetchNews()
        }
    }


    private fun initObserver(){
        homeViewModel.getNews.observe(viewLifecycleOwner) { list ->

            if(!Utils.isNetworkAvailable(requireContext())){
                if(list.isNullOrEmpty()){
                    Toast.makeText(requireContext(), requireContext().getString(R.string.do_not_have_connection_continue_news), Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(requireContext(), requireContext().getString(R.string.do_not_have_connection_news), Toast.LENGTH_SHORT).show()
                }
            }

            newsAdapter = NewsAdapter(list)

            newsAdapter.setOnclickElement {
                homeViewModel.addIndex(it)
                findNavController().navigate(Uri.parse("app://details"))

            }

            mBinding.rvNews.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = newsAdapter
            }
        }
    }


}
