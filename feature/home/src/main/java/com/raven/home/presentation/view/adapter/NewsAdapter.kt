package com.raven.home.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.raven.home.R
import com.raven.home.domain.models.GetNewsModel

class NewsAdapter (val list:List<GetNewsModel>) : RecyclerView.Adapter<NewsViewHolder>() {
    private var onClickElement: ((Int) -> Unit)? = null

    fun setOnclickElement(onClick: (Int) -> Unit) {
        this.onClickElement = onClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return NewsViewHolder(layoutInflater.inflate(R.layout.item_news_layout, parent, false))
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(list[position], position)
        holder.setOnclickItem = onClickElement
    }

    override fun getItemCount(): Int = list.size
}