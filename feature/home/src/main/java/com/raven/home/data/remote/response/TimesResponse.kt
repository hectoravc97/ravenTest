package com.raven.home.data.remote.response

import com.google.gson.annotations.SerializedName
import com.raven.home.data.model.ResultsResponseModel

data class TimesResponse(
    @SerializedName("status"      ) var status     : String?            = null,
    @SerializedName("copyright"   ) var copyright  : String?            = null,
    @SerializedName("num_results" ) var numResults : String?               = null,
    @SerializedName("results"     ) var results    : List<ResultsResponseModel> = arrayListOf()
)