package com.raven.home.domain.models

data class GetNewsModel (
    var uri           : String?           = null,
    var url           : String?           = null,
    var id            : String?              = null,
    var assetId       : String?              = null,
    var source        : String?           = null,
    var publishedDate : String?           = null,
    var updated       : String?           = null,
    var section       : String?           = null,
    var subsection    : String?           = null,
    var nytdsection   : String?           = null,
    var adxKeywords   : String?           = null,
    var column        : String?           = null,
    var byline        : String?           = null,
    var type          : String?           = null,
    var title         : String?           = null,
    var abstract      : String?           = null,
    var etaId         : String?              = null
)