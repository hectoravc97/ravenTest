package com.raven.home.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raven.home.domain.models.GetNewsModel
import com.raven.home.domain.usecases.GeNewsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch

import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val getNewsUseCase: GeNewsUseCase) : ViewModel(){

    private val _getNews = MutableLiveData<List<GetNewsModel>>()
    val getNews:  LiveData<List<GetNewsModel>> = _getNews

    private var index : Int = 0
    private var list : List<GetNewsModel> = emptyList()

    fun fetchNews(){
        viewModelScope.launch {
            getNewsUseCase.execute()
                .catch {
                    _getNews.value = emptyList()
                }
                .collect {
                    list = it
                    _getNews.value = it
            }
        }
    }


    fun addIndex(indextOf: Int){
        index = indextOf
    }

    fun getIndex(): Int{
        return index
    }

    fun getList(): List<GetNewsModel>{
        return list
    }
}
