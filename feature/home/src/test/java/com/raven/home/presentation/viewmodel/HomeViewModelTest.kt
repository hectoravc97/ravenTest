package com.raven.home.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.raven.home.domain.models.GetNewsModel
import com.raven.home.domain.usecases.GeNewsUseCase
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


val fakeNews = listOf(
    GetNewsModel("nyt://article/2b9e67d7-5ce8-514e-9179-86f852163f9f",
        "https://www.nytimes.com/2024/01/22/well/move/strength-30-second-power-test-aging.html",
        "100000009200691", "100000009200691", "New York Times", "2024-01-22","2024-02-05 19:20:27",
        "Well","Move","well","Content Type: Service;","null","By Amanda Loudin",
        "Article","Take the 30","You need more than strength","0")
)

class HomeViewModelTest {

    private lateinit var useCase: GeNewsUseCase

    private lateinit var homeViewModel: HomeViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun onBefore() {

        useCase = mockk()

        homeViewModel = HomeViewModel(useCase)

        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    @Test
    fun testNewsFail() = runBlocking {
        val list : List<GetNewsModel> = emptyList()

        coEvery { useCase.execute() } returns flowOf(list)

        homeViewModel.fetchNews()

        // expect
        useCase.execute().collectLatest { states ->
            val actual = homeViewModel.getNews.value
            val expected = states
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testIsCorrect() = runBlocking {
        val list : List<GetNewsModel> = fakeNews

        coEvery { useCase.execute() } returns flowOf(list)

        homeViewModel.fetchNews()

        // expect
        useCase.execute().collectLatest { states ->
            val actual = homeViewModel.getNews.value
            val expected = states
            assertEquals(expected, actual)

            assertEquals(expected[0].id, actual!![0].id)
        }
    }
}