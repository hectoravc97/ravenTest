package com.raven.home.domain.usecases

import com.raven.home.data.model.ResultsResponseModel
import com.raven.home.domain.HomeDataSource
import com.raven.home.domain.mapper.GetNewsEntityMapper
import com.raven.home.domain.mapper.GetNewsMapper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetNewsUseCaseTest {

    private lateinit var dataSource: HomeDataSource

    private lateinit var mapper: GetNewsMapper

    private lateinit var mapperEntity: GetNewsEntityMapper

    private lateinit var getNewsUseCase: GeNewsUseCase

    @Before
    fun onBefore() {

        dataSource = mockk()
        mapper = mockk()
        mapperEntity = mockk()

        getNewsUseCase = spyk(GeNewsUseCase(dataSource, mapper, mapperEntity))



        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }


    @Test
    fun testNewsRepositoryFail() = runBlocking {
        val list : List<ResultsResponseModel> = emptyList()

        coEvery { dataSource.getNews() } returns flowOf(list)

        getNewsUseCase.execute()

        // expect
        dataSource.getNews().collectLatest { states ->
            getNewsUseCase.execute().collect{
                val expected = states
                assertEquals(expected, it)
            }

        }
    }


    @Test
    fun testNewsRepositoryIsCorrect() = runBlocking {
        val list : List<ResultsResponseModel> = listOf(
            ResultsResponseModel("nyt://article/2b9e67d7-5ce8-514e-9179-86f852163f9f",
                "https://www.nytimes.com/2024/01/22/well/move/strength-30-second-power-test-aging.html",
                "100000009200691", "100000009200691", "New York Times", "2024-01-22","2024-02-05 19:20:27",
                "Well","Move","well","Content Type: Service;","null","By Amanda Loudin",
                "Article","Take the 30","You need more than strength","0")
        )

        coEvery { dataSource.getNews() } returns flowOf(list)


        getNewsUseCase.execute().collect{

            coVerify(exactly = 1) { dataSource.clearNews() }
            coVerify(exactly = 1) { dataSource.insertAllNews(any()) }
            coVerify(exactly = 0) { dataSource.getNewsFromDB() }

            assertEquals(list, it)
        }

    }

}